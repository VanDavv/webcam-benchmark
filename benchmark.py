import argparse
import os
import re
import sys
import tempfile
from statistics import mean

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--fps", default=30, type=int, help="desired target FPS")
parser.add_argument("-t", "--time", default=30, type=int, help="desired time of buffering/recording (in seconds)")
parser.add_argument("--oak", action="store_true")
args = parser.parse_args()

logitech_name = "C922 Pro Stream Webcam"
KB = 1024
MB = 1024 * KB


def find_logitech():
    device_names = []
    for file in os.listdir("/sys/class/video4linux"):
        real_file = os.path.realpath("/sys/class/video4linux/" + file + "/name")
        with open(real_file, "rt") as name_file:
            name = name_file.read().rstrip()
        device_names.append(name)
        if logitech_name in name:
            cam_num = int(re.search("\d+$", file).group(0))
            import cv2
            vid = cv2.VideoCapture(cam_num)
            if vid.read()[0]:
                print(f"Logitech {logitech_name} camera found!")
                return cam_num
            else:
                print(f"[WARN] Logitech {logitech_name} camera found but is inaccessible!")
    print(f"Logitech {logitech_name} camera not found!", file=sys.stderr)
    print("Available devices:", file=sys.stderr)
    for name in set(device_names):
        print(" - " + name, file=sys.stderr)
    raise SystemExit(1)


def parse_frames(frame_list):
    sizes = [frame.size for frame in frame_list]
    total_size = sum(sizes)
    avg_size = mean(sizes)
    return {
        "total": total_size,
        "avg": avg_size
    }


def print_result(results):
    print(f"AVG SIZE:   {results['avg'] / MB:.2f} [MB]")
    print(f"TOTAL SIZE: {results['total'] / MB:.2f} [MB]")
    print(f"BANDWIDTH:  {results['total'] * 8 / args.time / MB:.2f} [Mbps]")


if args.oak:
    print("OAK")
else:
    num = find_logitech()
    print()
    print("===STARTING ANALYSIS===")
    print(f"FPS:    {args.fps}")
    print(f"TIME:   {args.time} [s]")
    print(f"FRAMES: {args.fps * args.time}")
    import cv2

    vid = cv2.VideoCapture(num)

    frames = [
        vid.read()[1]
        for _ in range(args.time * args.fps)
    ]

    print()
    print("[RAW]")
    result = parse_frames(frames)
    print_result(result)

    print()
    print("[JPEG 90%]")
    encoded_frames = [
        cv2.imencode('.jpg', frame, [int(cv2.IMWRITE_JPEG_QUALITY), 90])[1]
        for frame in frames
    ]
    result = parse_frames(encoded_frames)
    print_result(result)

    print()
    print("[JPEG 80%]")
    encoded_frames = [
        cv2.imencode('.jpg', frame, [int(cv2.IMWRITE_JPEG_QUALITY), 80])[1]
        for frame in frames
    ]
    result = parse_frames(encoded_frames)
    print_result(result)

    print()
    print("[JPEG 10%]")
    encoded_frames = [
        cv2.imencode('.jpg', frame, [int(cv2.IMWRITE_JPEG_QUALITY), 10])[1]
        for frame in frames
    ]
    result = parse_frames(encoded_frames)
    print_result(result)

    print()
    print("[PNG 9]")
    encoded_frames = [
        cv2.imencode('.png', frame, [int(cv2.IMWRITE_PNG_COMPRESSION), 9])[1]
        for frame in frames
    ]
    result = parse_frames(encoded_frames)
    print_result(result)

    print()
    print("[MP4V]")
    file_out = tempfile.NamedTemporaryFile(suffix='.mp4')
    height, width = frames[0].shape[:2]
    video = cv2.VideoWriter(file_out.name, cv2.VideoWriter_fourcc(*'mp4v'), 20, (width, height))

    for frame in frames:
        video.write(frame)

    video.release()
    size = os.stat(file_out.name).st_size
    print(f"TOTAL SIZE: {size / MB:.2f} [MB]")
    print(f"BANDWIDTH:  {size * 8 / args.time / MB:.2f} [Mbps]")

    print()
    print("[VP9]")
    file_out = tempfile.NamedTemporaryFile(suffix='.webm')
    height, width = frames[0].shape[:2]
    video = cv2.VideoWriter(file_out.name, cv2.VideoWriter_fourcc(*'vp09'), 20, (width, height))

    for frame in frames:
        video.write(frame)

    video.release()
    size = os.stat(file_out.name).st_size
    print(f"TOTAL SIZE: {size / MB:.2f} [MB]")
    print(f"BANDWIDTH:  {size * 8 / args.time / MB:.2f} [Mbps]")


    print()
    print("[AVI]")
    file_out = tempfile.NamedTemporaryFile(suffix='.avi')
    height, width = frames[0].shape[:2]
    video = cv2.VideoWriter(file_out.name, cv2.VideoWriter_fourcc(*'XVID'), 20, (width, height))

    for frame in frames:
        video.write(frame)

    video.release()
    size = os.stat(file_out.name).st_size
    print(f"TOTAL SIZE: {size / MB:.2f} [MB]")
    print(f"BANDWIDTH:  {size * 8 / args.time / MB:.2f} [Mbps]")
