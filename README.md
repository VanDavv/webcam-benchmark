# Benchmark for Logitech Webcam (+ additional benchmark for OpenCV AI Kit camera)

Results from running on x86_64 Linux with Logitech C922 Pro Stream Webcam connected via USB2 cable

```
/home/vandavv/dev/webcam-benchmark/venv/bin/python /home/vandavv/dev/webcam-benchmark/benchmark.py 
[WARN] Logitech C922 Pro Stream Webcam camera found but is inaccessible!
[ WARN:0@0.005] global /io/opencv/modules/videoio/src/cap_v4l.cpp (902) open VIDEOIO(V4L2:/dev/video3): can't open camera by index
Logitech C922 Pro Stream Webcam camera found!

===STARTING ANALYSIS===
FPS:    30
TIME:   30 [s]
FRAMES: 900

[RAW]
AVG SIZE:   0.88 [MB]
TOTAL SIZE: 791.02 [MB]
BANDWIDTH:  210.94 [Mbps]

[JPEG 90%]
AVG SIZE:   0.05 [MB]
TOTAL SIZE: 43.69 [MB]
BANDWIDTH:  11.65 [Mbps]

[JPEG 80%]
AVG SIZE:   0.03 [MB]
TOTAL SIZE: 27.88 [MB]
BANDWIDTH:  7.43 [Mbps]

[JPEG 10%]
AVG SIZE:   0.01 [MB]
TOTAL SIZE: 7.54 [MB]
BANDWIDTH:  2.01 [Mbps]

[PNG 9]
AVG SIZE:   0.40 [MB]
TOTAL SIZE: 356.86 [MB]
BANDWIDTH:  95.16 [Mbps]

[MP4V]
OpenCV: FFMPEG: tag 0x39307076/'vp09' is not supported with codec id 167 and format 'webm / WebM'
TOTAL SIZE: 5.97 [MB]
BANDWIDTH:  1.59 [Mbps]

[VP9]
TOTAL SIZE: 40.56 [MB]
BANDWIDTH:  10.82 [Mbps]

[AVI]
TOTAL SIZE: 5.99 [MB]
BANDWIDTH:  1.60 [Mbps]

Process finished with exit code 0


Process finished with exit code 0
```

## Installation

```
$ python3 install.py
```

## Usage

For Logitech camera:

```
$ python3 benchmark.py
```

For OAK camera:

```
$ python3 benchmark.py --oak
```